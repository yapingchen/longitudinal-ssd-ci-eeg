%% ADD PATHS FOR FIELTRIPTOOLBOX 
addpath('/the_fieldtrip_path');

%% load healthy subs' data
indir = '../figure3/data_NHC/';
all_mat_files = functions.get_mat_files(indir);

ind_aud_files = [1:2:20];
ind_ci_files = [2:2:20];
all_aud_files = all_mat_files(ind_aud_files);
all_ci_files = all_mat_files(ind_ci_files);

for subi = 1:length(all_aud_files)
    load(all_aud_files{subi})
    healthy_aud_RDM(subi,:,:,:) = RDM;
end

for subi = 1:length(all_ci_files)
    load(all_ci_files{subi})
    healthy_ci_RDM(subi,:,:,:) = RDM;
end

% switch it depends on correlating to nonCI or CI: healthy_aud_RDM, healthy_ci_RDM
healthy_RDM = healthy_aud_RDM; 

% choose time window of interest
time_begin = -0.2; time_end = 1.2;
ind_time_begin = find(time == time_begin);
ind_time_end = find(time == time_end);

healthy_RDM_select = healthy_RDM(:,:,:,ind_time_begin:ind_time_end);
time_select = time(ind_time_begin:ind_time_end);

%% load CI-CI data
indir = '../figure3/data_CI/';
all_mat_files = functions.get_mat_files(indir);

% ci ear
op1 = [1:10]; op2 = [11:20]; op3 = [21:30];
sess_files = [op1;op2;op3];
all_sess = 3;
all_sess_ci_RDM = cell(1,all_sess);
all_sess_mean_ci_RDM = cell(1,all_sess);
sess_ind = 1;

for sess = 1:all_sess
    
    curr_mat_files = all_mat_files(sess_files(sess,:));
    for subi = 1:length(curr_mat_files)
        load(curr_mat_files{subi})
        ci_RDM(subi,:,:,:) = RDM;
    end
    % choose time window of interest
    time_begin = -0.1; time_end = 1.3; %%% time corrected according to CI artefact spike
    ind_time_begin = 21; % find(time == time_begin);
    ind_time_end = find(time == time_end);
    
    ci_RDM_select = ci_RDM(:,:,:,ind_time_begin:ind_time_end);
    time_select = time(ind_time_begin:ind_time_end);
    
    all_sess_ci_RDM{sess_ind} = ci_RDM_select;
    all_sess_mean_ci_RDM{sess_ind} = squeeze(mean(ci_RDM_select,1));
    sess_ind = sess_ind+1;
end

%%%%
time_begin = -0.2; time_end = 1.2;
ind_time_begin = find(time == time_begin);
ind_time_end = find(time == time_end);
time_select = time(ind_time_begin:ind_time_end);

%% comopute healthy-CI-CI time-generalization correlation matrix
% Pre-allocate result matrix
rsaResMat = nan(10,length(time_select),length(time_select));
ci_op1 = all_sess_mean_ci_RDM{1};
ci_op2 = all_sess_mean_ci_RDM{2};
ci_op3 = all_sess_mean_ci_RDM{3};
% We correlated RDMs (i.e., calculating the Spearman’s R)
% in CI ears (average across participants) and normal hearing control (for each participant separately)
% for all time point combinations (tx, ty), assigning the values to a time-generalization
% matrix indexed in rows and columns by the time in adults (tx) and infants (ty).

ci_RDM = ci_op1; % ci_op1, ci_op2, ci_op3

for subj = 1:10
    for time_ci = 1:71 % Loop through time points
        % Vectorize RDMs
        vect_ci = functions.vectorizerdm(squeeze(ci_RDM(:,:,time_ci)));
        
        for time_healthy = 1:71 % Loop through time points
            % Vectorize RDMs
            vect_healthy = functions.vectorizerdm(squeeze(healthy_RDM_select(subj,:,:,time_healthy))); 
            % Correlation
            rsaResMat(subj,time_ci,time_healthy) = functions.correlatevectors(vect_ci,vect_healthy);
        end
    end
end

% Display run time
disp("RSA done.")

%% CI-aud
indir = '../figure3/data_nonCI/'; 
all_mat_files = functions.get_mat_files(indir);

% non-CI ear
pre = [31:40]; op1 = [1:10]; op2 = [11:20]; op3 = [21:30];
sess_files = [pre;op1;op2;op3];
all_sess = 4;
all_sess_RDM = cell(1,all_sess);

sess_ind = 1;

for sess = 1:all_sess
 
    curr_mat_files = all_mat_files(sess_files(sess,:));
    for subi = 1:length(curr_mat_files)
        load(curr_mat_files{subi})
        aud_RDM(subi,:,:,:) = RDM;
    end
    
    % choose time window of interest
    time_begin = -0.2; time_end = 1.2;
    ind_time_begin = find(time == time_begin);
    ind_time_end = find(time == time_end);
    
    aud_RDM_select = aud_RDM(:,:,:,ind_time_begin:ind_time_end);
    time_select = time(ind_time_begin:ind_time_end);
    
    all_sess_aud_RDM{sess_ind} = aud_RDM_select;
    all_sess_mean_aud_RDM{sess_ind} = squeeze(mean(aud_RDM_select,1));
    sess_ind = sess_ind+1;
    
end

%% Pre-allocate result matrix
% clear aud_RDM
% rsaResMat = nan(10,101,101);
rsaResMat = nan(10,71,71);
aud_pre = all_sess_mean_aud_RDM{1};
aud_op1 = all_sess_mean_aud_RDM{2};
aud_op2 = all_sess_mean_aud_RDM{3};
aud_op3 = all_sess_mean_aud_RDM{4};
% Compare RDMs (i.e., calculating the Spearman’s R)
% in nonCI ears (average across participants) and healthy controls (average across participants)
% for all time point combinations (tx, ty), assigning the values to a time-generalization
% matrix indexed in rows and columns by the time in adults (tx) and infants (ty).
aud_RDM = aud_op3;

for subj = 1:10
    for time_aud = 1:71 % Loop through time points % 101, 71
        % Vectorize RDMs
        vect_aud = functions.vectorizerdm(squeeze(aud_RDM(:,:,time_aud)));
        
        for time_healthy = 1:71 % Loop through time points % 101, 71
            % Vectorize RDMs
            vect_healthy = functions.vectorizerdm(squeeze(healthy_RDM_select(subj,:,:,time_healthy))); % healthy_RDM, healthy_RDM_select
            % Correlation
            rsaResMat(subj,time_aud,time_healthy) = functions.correlatevectors(vect_aud,vect_healthy);
        end
    end
end

% Display run time
disp("RSA done.")

%% fisher z transformed rsaResMat
rsaResMat(rsaResMat ==1) = 0.999;
rsaResMat(rsaResMat ==-1) = -0.999;

rsaResMat_z = atanh(rsaResMat);
rsaResMat_orig = rsaResMat;
rsaResMat = rsaResMat_z;

%% Prepare for stats - create fake tf-structure
clear fakeTF
Fs = 50; % sampling rate
times_healthy = time_select; 
times_CI = time_select; 
tmpMat(:,1,:,:) = rsaResMat;

fakeTF = [];
fakeTF.fsample = Fs;
fakeTF.powspctrm = tmpMat;
fakeTF.label = {'accuracy'};
fakeTF.time = times_healthy;
fakeTF.freq= times_CI;
fakeTF.dimord = 'subj_chan_freq_time'; 

data4stat = fakeTF;

zeros4stat = data4stat;
zeros4stat.powspctrm = zeros(size(tmpMat));

%% CALCULATE STATISTIC
clear stat
cfg = [];
cfg.channel          = {'accuracy'};
cfg.method           = 'montecarlo'; 
cfg.statistic        = 'ft_statfun_depsamplesT';
cfg.correctm         = 'cluster';
cfg.clusterstatistic = 'maxsum';
cfg.numrandomization = 10000; 
cfg.clustertail      = 1; 
cfg.tail             = 1; 
cfg.clusteralpha     = 0.05;
cfg.alpha            = 0.05;

% prepare design
n = size(data4stat.powspctrm,1);
design = zeros(2,n*2);
design(1,1:n) = 1:n;
design(1,n+1:end) = 1:n;
design(2,1:n) = 1;
design(2,n+1:end) = 2;

cfg.design   = design;
cfg.uvar     = 1;
cfg.ivar     = 2;

stat = ft_freqstatistics(cfg, data4stat, zeros4stat);

%% fieldtrip plot
origMat(:,1,:,:) = rsaResMat_orig;
data4stat.powspctrm = origMat;
data4stat.mask = stat.mask; 
cfg = [];
cfg.zlim         = [-0.55 0.55]; 
cfg.maskparameter = 'mask';
cfg.maskstyle = 'outline';
stat_fig = ft_singleplotTFR(cfg, data4stat); 
