%% plot decoding time course of normal hearing control
% average the lower triangular part of the decoding matrix, resulting in grand average decoding accuracy (i.e., decoding accuracies across repetitions)
% as a measure of how well intelligibility is discriminated by audio representations at a particular time point.
indir = './data_NHC/'; % degradation_rdm_long_v3_8, degradation_rdm_long_loso_right_p100t10
all_mat_files = functions.get_mat_files(indir);
ind_aud_files = [1:2:20]; ind_aud_files = ind_aud_files(1:10);
ind_ci_files = [2:2:20]; ind_ci_files = ind_ci_files(1:10);
all_aud_files = all_mat_files(ind_aud_files);
all_ci_files = all_mat_files(ind_ci_files);

for subi = 1:length(all_aud_files)    
    load(all_aud_files{subi})
    aud_all(subi,:,:,:) = DA_mean;     
end

for subi = 1:length(all_ci_files)    
    load(all_ci_files{subi})
    ci_all(subi,:,:,:) = DA_mean;    
end

acc_all = (aud_all + ci_all)/2;
% acc_all = aud_all;
% acc_all = ci_all;

% choose time window of interest
time_begin = -0.2; time_end = 1.2;
ind_time_begin = find(time == time_begin);
ind_time_end = find(time == time_end);
time_select = time(ind_time_begin:ind_time_end);
acc_select = acc_all(:,:,:,ind_time_begin:ind_time_end);

acc_mat = squeeze(nansum(squeeze(nansum(acc_select,2)),2))/6;

gravg_acc = squeeze(nanmean(acc_select,1));

gravg_acc_sum_cond = squeeze(nansum(squeeze(nansum(gravg_acc,1)),1)); 
TimeCourse = gravg_acc_sum_cond/6; 
TimeCourse = smooth(TimeCourse,6); 

figure;
colorcodeH = [0 0 0]; 
[h, p] = functions.boundedline(time_select, TimeCourse, std(acc_mat,[],1)/sqrt(size(acc_mat,1)), 'b', 'alpha');
h.Color = colorcodeH;
h.LineWidth = 2;
p.FaceColor = colorcodeH;
plot([time_select(1), time_select(end)],[50, 50],'k--') % chance level
xlabel('Time (ms)'); ylabel('Decoding accuracy (%)');
ylim([46, 62])

%% plot chance level
hold on
plot([time_select(1,1), time_select(1,end)],[50, 50],'k--')

%% plot decoding time course of CI's non-CI ear
% get data
indir = './data_nonCI/';
all_mat_files = functions.get_mat_files(indir);
acc_mat = cell(1,4); gravg_acc = cell(1,4); TimeCourse = cell(1,4);

pre = [31:40]; op1 = [1:10]; op2 = [11:20]; op3 = [21:30];

sess_files = [pre;op1;op2;op3];

time = [-0.5:0.02:(1.5-0.02)];

for sess = 1:4
    curr_mat_files = all_mat_files(sess_files(sess,:));
    for subi = 1:length(curr_mat_files)
        load(curr_mat_files{subi})
        acc_all(subi,:,:,:) = DA_mean;
    end
        
    % choose time window of interest
    time_begin = -0.2; time_end = 1.2;
    ind_time_begin = find(time == time_begin);
    ind_time_end = find(time == time_end);
    time_select = time(ind_time_begin:ind_time_end);
    acc_select = acc_all(:,:,:,ind_time_begin:ind_time_end);
    
    acc_mat{sess} = squeeze(nansum(squeeze(nansum(acc_select,2)),2))/6;
    gravg_acc{sess} = squeeze(nanmean(acc_select,1));
    TimeCourseSum{sess} = squeeze(nansum(squeeze(nansum(gravg_acc{sess},1)),1));
    TimeCourse{sess} = TimeCourseSum{sess}/6;
end

% plot settings 
colorcode = zeros(4,3);
colorcode(1,:) = [252, 231, 0]/255; colorcode(2,:) = [124, 209, 70]/255;
colorcode(3,:) = [40, 168, 130]/255; colorcode(4,:) = [44, 120, 143]/255;

% figure;
for sess_ind = 1:4    
    TimeCourseResult = smooth(TimeCourse{sess_ind},6);
    [h, p] = boundedline(time_select, TimeCourseResult, std(acc_mat{sess_ind},[],1)/sqrt(10), 'b', 'alpha');
    h.Color = colorcode(sess_ind,:);
    h.LineWidth = 2;
    p.FaceColor = colorcode(sess_ind,:);
    hold on
end

xlabel('Time (ms)'); ylabel('Decoding accuracy (%)');
title('Time course of intellgibility decoding in non-CI ear');

%% plot decoding time course of CI's CI ear
indir = './data_CI/';
all_mat_files = functions.get_mat_files(indir);
gravg_acc = cell(1,3); TimeCourseSum = cell(1,3); TimeCourse = cell(1,3); acc_mat = cell(1,3);
op1 = [1:10]; op2 = [11:20]; op3 = [21:30];

sess_files = [op1;op2;op3];

for sess = 1:3
    curr_mat_files = all_mat_files(sess_files(sess,:));
    for subi = 1:length(curr_mat_files)
        load(curr_mat_files{subi})
        acc_all(subi,:,:,:) = DA_mean; 
    end
    
    time_begin = -0.2; time_end = 1.2;
    ind_time_begin = find(time == time_begin);
    ind_time_end = find(time == time_end);
    time_select = time(ind_time_begin:ind_time_end);
    acc_select = acc_all(:,:,:,ind_time_begin:ind_time_end);
    
    acc_mat{sess} = squeeze(nansum(squeeze(nansum(acc_select,2)),2))/6;

    gravg_acc{sess} = squeeze(nanmean(acc_select,1));
    TimeCourseSum{sess} = squeeze(nansum(squeeze(nansum(gravg_acc{sess},1)),1));
    TimeCourse{sess} = TimeCourseSum{sess}/6; 
end

colorcode = zeros(3,3);
colorcode(1,:) = [124, 209, 70]/255;
colorcode(2,:) = [40, 168, 130]/255; 
colorcode(3,:) = [44,120, 143]/255;

% figure;
for sess_ind = 1:3
    TimeCourseResult = smooth(TimeCourse{sess_ind},6);
    [h, p] = functions.boundedline(time_select, TimeCourseResult, std(acc_mat{sess_ind},[],1)/sqrt(10), 'b', 'alpha');
    h.Color = colorcode(sess_ind,:);
    h.LineWidth = 2;
    p.FaceColor = colorcode(sess_ind,:);
    hold on
end

plot([time_select(1), time_select(end)],[50, 50],'k--') % chance level

xlabel('Time (ms)'); ylabel('Decoding accuracy (%)');
title('Time course of intellgibility decoding in CI ear');

