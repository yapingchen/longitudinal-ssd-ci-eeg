function [all_mat_files] = get_mat_files(indir)

%% get all mat file from the indir foler

datadir = fullfile(indir, '/*.mat');

D = dir(datadir);
if length(D) <1
  error(['no fif-files found in',datadir])
end

all_mat_files = cell(1,length(D));
for i = 1:length(D) % loop through all mat-file names
  ifilename = D(i).name;
  ifolder = D(i).folder;

  all_mat_files{i} = fullfile(ifolder, ifilename); 
  
end

