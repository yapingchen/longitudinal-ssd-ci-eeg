function yall = ShiftingVocode(InputFile, OutputFile, LoFreq, HiFreq, nChannels, mapping, shift, threshold, filters, rectification, ...
   			smoothing, excitation, BandBegin, BandEnd) ;

%  ShiftingVocode: A program to do noise- and sine-wave vocoding which
%   also allows the shifting of the output spectra. This version
%   essentially the same as CHARVEY, which was a modified version of CLARE,
%   which was modified from DEBBIE4, written to generate the shifted stimuli
% 	for the summer 2000 project of Clare Norman. DEBBIE4 was written especially to generate the
%	stimuli required for the Summer, 1999 Wellcome Trust Vacation Project
% 	run by D Stanton, and itself was modified from VOCODE4. DEBBIE4 was also used for the student
%	undergraduate project of Jessica Bird 1999-2000
%	
%  ShiftingVocode reconstructs speech as a sum of 'nChannels' noise bands 
%	or sinusoids, with the possibility of spectrally shifting the reconstructed stimulus 
%	(for example, by a given number of mm on the basilar membrane). 
%	There is also the option of outputting an arbitrary number of contiguous bands of the complete spectrum.
%	A final option is to adjust the overall level of each band in the shifted condition by the Minimum Audible Field.
%
%  Changes here relative to CLARE:
%		No more low-pass filtering of the final output waveform at a cutoff of HiFreq --
%		Argument order is different.c
%		Waves are upsampled to 44.1 kHz for processing, but then downsampled to the original rate
%	This version works only with MATLAB 5, as it uses the .wav read and write
%	It also assumes a .wav file, and will handle no other formats.
%
%	ShiftingVocode(InputFile, OutputFile, LoFreq, HiFreq, nChannels, mapping, shift, threshold, filters, rectification, ...
%   			smoothing, excitation, BandBegin, BandEnd)	
%
%   InputFile - input filename in quotes, e.g., 'heed.wav'
%   OutputFile- output filename in quotes, e.g., 'heed2.wav'
%	LoFreq - lower edge of lowest analysis filter  
%	HiFreq - upper edge of highest analysis filter -- the input waveform is low-pass filtered at this value. 
%   nChannels - the number of channels
%	mapping of channels - 'n'(ormal) or 'i'(nverted)
%	shift - A positive number indicates the degree of shift in the reconstruction
%		(0 indicates no shift). A negative number indicates a shift downward.
%	threshold - 0->1 - correct for threshold differences with a proportion of MAF, but only in shifted conditions
%	filters - 'greenwood', 'linear', 'mel' or 'log'
%	rectification - 'half' or 'full'
%	smoothing (filter cutoff frequency)
%	excitation - 'noise' or 'sine'
%	BandBegin - starting band number to output. <=0 for default of all bands summed
%	BandEnd - finishing band number to output. <=0 for default of all bands summed
%
%	For example:
%   Ordinary 6-channel vocoding:
% 	ShiftingVocode('0101.WAV','0101_6.WAV',70,4000,6,'n',0,0,'greenwood','half',400,'noise',0,0)
%   6.46 mm shift on the basilar membrane with threshold correction
%   ShiftingVocode('0101.WAV','0101_6.WAV',70,4000,6,'n',6.46,0.5,'greenwood','half',400,'noise',0,0)
%
%	Stuart Rosen July 2006
%	stuart@phon.ucl.ac.uk

DEBUG=0;

if nargin<5
	fprintf('Insufficient number of parameters.\nType: help charvey for more help.\n\n');
	return;
end

if nargin<14
	BandEnd=nChannels;
end
if nargin<13
	BandBegin=1;
end
if nargin<12
	excitation='noise';
end
if nargin<11
	smoothing=400;
end
if nargin<10
	rectification='half';
end
if nargin<9
	filters='greenwood';
end
if nargin<8
	threshold=0;
end
if nargin<7
	shift=0;
end
if nargin<6
	mapping='n';
end

if HiFreq<=0
   HiFreq=4000;
end

if nChannels<=0
	fprintf('The number of channels has to be greater than 0.\n\n'); 
   return;
end

if smoothing<=0
	fprintf('The low pass cutoff of the smoothing filter must be greater than 0.\n\n'); 
	return;
end

if threshold<0 | threshold>1
   fprintf('The threshold weighting proportion must be between 0 and 1.\n\n');
end
   
if BandBegin<=0 | BandBegin>nChannels
	BandBegin=1;
end
if BandEnd<=0 | BandEnd>nChannels
	BandEnd=nChannels;
end

fprintf('%s -> %s, bands %d->%d of %d with shift= %g, %s filters over %g - %g Hz\n', ...
		InputFile, OutputFile, BandBegin, BandEnd, nChannels, shift, filters, LoFreq, HiFreq);

% Get the input waveform and the sampling frequency (SampRate) --
[x, SampRate, Nbits] =  wavread(InputFile);
% Upsample to 44.1 kHz if necessary
OriginalSampRate = SampRate;
if SampRate==22050
   x = resample(x, 2, 1);
   SampRate=44100;
elseif SampRate==16000
   x = resample(x, 441, 160);
   SampRate=44100;
elseif SampRate==44100
else
   error('Sampling rate must be one of 16kHz, 22.05 kHz or 44.1 kHz');
end

srat2=SampRate/2;
n_samples=length(x);
% calculate input level, in terms of root sum squared
InputLevel=norm(x);

%---------------------Design the filters ----------------
%
% first, do it for no mapping included
[filterA,filterB,center,valid]=estfilt2(nChannels,filters,LoFreq,HiFreq,SampRate,0,DEBUG);
% Give an indication if some filters are excluded
if valid<nChannels
   fprintf('WARNING! only %d of %d channels have been implemented\n', valid, nChannels);
end
% fprintf('Centre frequencies: \n'); fprintf('%6.0f ', center); fprintf('\n');

% second, if necessary, calculate values for a shifted output
if shift~=0
	[mfilterA,mfilterB,mcenter,mvalid]=estfilt2(nChannels,filters,LoFreq,HiFreq,SampRate,shift,DEBUG);
	% Give an indication if some filters are excluded
	if mvalid<nChannels
   	fprintf('WARNING! only %d of %d shifted channels have been implemented\n', mvalid, nChannels);
	end
	%fprintf('Shifted frequencies: \n'); fprintf('%6.0f ', mcenter); fprintf('\n');
	% save away original center frequencies for use in threshold corrections
	ocenter = center;
end

% --- design low-pass envelope filter ----------
%
% [blo,alo]=butter(6, 0.4);
% % JONAS CHANGE: was originally:
[blo,alo]=butter(2, smoothing/srat2);

% create buffers for the necessary waveforms
% 'x' is the original input waveform
% 'y' contains a single output waveform, 
%		the original after filtering through a bandpass filter
% 'ModCarriers' contains the complete set of nChannel modulated white noises or 
%  	sine waves, created by low-pass filtering the 'y' waveform, 
% 		and multiplying the resultant by an appropriate carrier
% 'band' contains the waveform associated with a single output channel, the modulated white
%		noise or sinusoid after filtering
% 'wave' contains the final output waveform constructing by adding together the ModCarriers,
%		which are first filtered by a filter matched to the input filter
%

ModCarriers =zeros(nChannels,n_samples);
y=zeros(1,n_samples);
wave=zeros(1,n_samples);
band=zeros(1,n_samples);



% re-set the centre frequencies so that sinusoidal carriers are synthesised
% at the right frequencies
if shift~=0
	center = mcenter;
end
% ----------------------------------------------------------------------%
% First construct the component modulated carriers for all channels  	%
% ----------------------------------------------------------------------%
for i=BandBegin:min(BandEnd, valid)
   % filter the original waveform into one channel
	y=filter(filterB(i,:),filterA(i,:),x)';
	% calculate its level
	level(i)=norm(y,2);
    
%     % JONAS add extra: extra-filter
%     [b,a] = cheby1(6,28,0.04, 'high');
%     y= filtfilt(b,a,y);

	% rectify and lowpass filter the channel filter output, to obtain an envelope
	if strcmp(rectification,'half') == 1 
		%-- half-wave rectify and smooth the filtered signal
		y=filter(blo,alo,0.5*(abs(y)+y));
	elseif strcmp(rectification,'full') == 1
		%-- full-wave rectify and smooth the filtered signal
		y=filter(blo,alo,abs(y));
	else fprintf('\nERROR! Rectification must be half or full\n');
		return;
    end

    % JONAS ADDITION: log y to a matrix with all channels' y
    yall(i,:) = y;

	if strcmp(excitation,'noise') == 1 
		% -- excite with noise ---
		ModCarriers(i,:) =y.*sign(rand(1,n_samples)-0.5);
	elseif strcmp(excitation, 'sine')==1  
		% ---- multiply by a sine wave carrier of the appropriate carrier ----
		if strcmp(mapping,'n') == 1 
			ModCarriers(i,:) =y.*sin(center(i)*2.0*pi*[0:(n_samples-1)]/SampRate); 
		elseif strcmp(mapping,'i') == 1 
			ModCarriers(i,:) =y.*sin(center((nChannels+1)-i)*2.0*pi*[0:(n_samples-1)]/SampRate); 
		else fprintf('\nERROR! Mapping must be n or i\n');
			return;
		end
	else fprintf('\nERROR! Excitation must be sine or noise\n');
		return;
	end
end

% ----------------------------------------------------------------------%
% Now filter the components (both noise and sine), and add together 	%
% into the appropriate order, scaling for equal rms per channel   	%
% ----------------------------------------------------------------------%

% re-set filter properties for mapping
if shift~=0
	filterA = mfilterA;
   filterB = mfilterB;
   valid = mvalid;
end

for i=BandBegin:min(BandEnd, valid)
	if strcmp(mapping,'n') == 1 
		band=filter(filterB(i,:),filterA(i,:), ModCarriers(i,:));
		% scale component output waveform to have 
		% equal rms to input component
		band=band*level(i)/norm(band,2);
      % also, if shifted, scale by 1/2 the threshold difference between the original channel and the new one   
      if shift~=0	& threshold>0   
         adj = threshold * (maf(mcenter(i))-maf(ocenter(i)));
         band = band * 10^(adj/20);
      end   
      elseif strcmp(mapping,'i') == 1 
		band=filter(filterB((nChannels+1)-i,:),filterA((nChannels+1)-i,:), ModCarriers(i,:));
		% scale component output waveform to have 
		% equal rms to input component
		band=band*level((nChannels+1)-i)/norm(band,2);
	end

	wave=wave+band;  % accumulate waveforms

end

% downsample back to the original rate 
if OriginalSampRate==22050
   wave = resample(wave, 1, 2);
   SampRate=22050;
elseif OriginalSampRate==16000
   wave = resample(wave, 160, 441);
   SampRate=22050;
end

% Think about some kind of amplitude normalization to bring up the level
% of waveforms that consist mainly of higher frequency channels.
% Try matching to input rms -- that would help anyway.
% k = InputLevel/norm(wave);
% wave = k * wave;
% 
% % ensure that no clipping will occur
% wave = wave2big(wave, 'From ShiftingVocode:');
% % ------------ now save output to a file -----------
% wavwrite(wave, SampRate, Nbits, OutputFile);


wave=my_normalize(wave, -24, 'r');
wavwrite(wave,SampRate,Nbits,OutputFile);


